import logo from "./logo.svg";
import "./App.css";
import { useState, useEffect } from "react";

function App() {
  const [form, setForm] = useState({
    name: "",
    email: "",
    password: "",
    address: "",
    interest: [],
    city: "",
    job: "",
    salary: "",
    favFood: "",
  });

  const [step, setStep] = useState(1);
  console.log(
    "checkbox",
    form.interest.find((item) => item === "bike")
  );

  console.log(
    "checkbox filter",
    form.interest.filter((item) => item !== "bike")
  );

  useEffect(() => {
    if (step > 3) {
      alert("register successs");
    }
  }, [step]);

  console.log("form", form);

  const changeForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const changeInterest = (e) => {
    if (form.interest.length <= 5) {
      setForm({
        ...form,
        interest: form.interest.find((item) => item === e.target.value)
          ? form.interest.filter((item) => item !== e.target.value)
          : [...form.interest, e.target.value],
      });
    }
  };
  const changeStep = (type) => {
    if (type === "next") {
      setStep(step + 1);
    } else {
      setStep(step - 1);
    }
  };

  return (
    <div className="App">
      {/* // step 1 */}
      {step === 1 ? (
        <div>
          <label>Name</label>
          <input
            type="text"
            placeholder="enter your name"
            value={form.name}
            name="name"
            onChange={(e) => changeForm(e)}
          />
          <label>email</label>
          <input
            type="text"
            placeholder="enter your email"
            value={form.email}
            name="email"
            onChange={(e) => changeForm(e)}
          />
          <label>password</label>
          <input
            type="password"
            placeholder="enter your password"
            value={form.password}
            name="password"
            onChange={(e) => changeForm(e)}
          />
          <button onClick={() => changeStep("next")}>Next</button>
        </div>
      ) : null}
      {step === 2 ? (
        <div>
          {/* // step 2 */}
          <label>Address</label>
          <input
            type="text"
            placeholder="enter your address"
            value={form.address}
            name="address"
            onChange={(e) => changeForm(e)}
          />
          <h4>Interest</h4>
          <label>bike</label>
          <input
            type="checkbox"
            placeholder="enter your interest"
            value="bike"
            onChange={(e) => changeInterest(e)}
            disabled={
              form.interest.length < 5
                ? false
                : form.interest.filter((item) => item !== "bike").length === 5
                ? true
                : false
            }
          />
          <label>computer</label>
          <input
            type="checkbox"
            placeholder="enter your interest"
            value="computer"
            onChange={(e) => changeInterest(e)}
            disabled={
              form.interest.length < 5
                ? false
                : form.interest.filter((item) => item !== "computer").length ===
                  5
                ? true
                : false
            }
          />
          <label>sports</label>
          <input
            type="checkbox"
            placeholder="enter your interest"
            value="sports"
            onChange={(e) => changeInterest(e)}
            disabled={
              form.interest.length < 5
                ? false
                : form.interest.filter((item) => item !== "sports").length === 5
                ? true
                : false
            }
          />
          <label>music</label>
          <input
            type="checkbox"
            placeholder="enter your interest"
            value="music"
            onChange={(e) => changeInterest(e)}
            disabled={
              form.interest.length < 5
                ? false
                : form.interest.filter((item) => item !== "music").length === 5
                ? true
                : false
            }
          />
          <label>art</label>
          <input
            type="checkbox"
            placeholder="enter your interest"
            value="art"
            onChange={(e) => changeInterest(e)}
            disabled={
              form.interest.length < 5
                ? false
                : form.interest.filter((item) => item !== "art").length === 5
                ? true
                : false
            }
          />
          <label>travelling</label>
          <input
            type="checkbox"
            placeholder="enter your interest"
            value="travelling"
            onChange={(e) => changeInterest(e)}
            disabled={
              form.interest.length < 5
                ? false
                : form.interest.filter((item) => item !== "travelling")
                    .length === 5
                ? true
                : false
            }
          />
          <label>City</label>
          <input
            type="text"
            placeholder="enter your city"
            value={form.city}
            name="city"
            onChange={(e) => changeForm(e)}
          />
          <button onClick={() => changeStep("prev")}>Prev</button>
          <button onClick={() => changeStep("next")}>Next</button>
        </div>
      ) : null}
      {/* // step 3 */}
      {step === 3 ? (
        <div>
          <label>Job</label>
          <input
            type="text"
            placeholder="enter your job"
            value={form.job}
            name="job"
            onChange={(e) => changeForm(e)}
          />
          <label>Salary</label>
          <input
            type="text"
            placeholder="enter your salary"
            value={form.salary}
            name="salary"
            onChange={(e) => changeForm(e)}
          />
          <label>Favorite Food</label>
          <input
            type="password"
            placeholder="enter your favorite food"
            value={form.favFood}
            name="favFood"
            onChange={(e) => changeForm(e)}
          />
          <button onClick={() => changeStep("prev")}>Prev</button>
          <button onClick={() => changeStep("next")}>Submit</button>
        </div>
      ) : null}
    </div>
  );
}

export default App;
